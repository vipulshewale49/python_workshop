#day1
#variable and assignment
#variable
#valid
#1)starting with char
#2)starting with underscore
#3)starting char followed by no.
#a=49
#print(a)
#_a=49
#print(_a)
#a3=49
#print(a3)
#note:it is recommende that prefer cammel case for creating variable
#####################
#assignment
#python supports multiple assignments
#a,b=23,45
#print(a,b)
# a=46
# b=23
# a,b=b,a
# print(a,b)
##############
#number system
# a=23
# b=45.87
# c=23+45j
# print(a,b,c)
# print(type(a),type(b),type(c))
###########
#string
# a='hello all'
# b="good evening"
# c='''enjoy your tea'''
# d="""ready for rbg system"""
# print(a,b,c,d,type(a))
##########
#list
# list1=[1,2,3,4,5]
# list2=[2,"hello",[2,3,4]]
# print(list1)
# print(list2)
# print(type(list1))
###########
#touple
# tup1=(1,2,3,4,5)
# tup2=(2,"hello",(2,4,3))
# print(tup1)
# print(tup2)
# print(type(tup2))
########
#dictionary
# dict1={"name":"vipul",
#          "std":"se",
#          "rollno":50,
#          "course":["java","python"]}
# print(dict1)
# print(type(dict1))
##########
#set
# set1={1,2,3,4,5}
# set2={2,3,4,5,6}
# print(set1.issubset(set2))
# print(set1)
# print(set2)
# print(type(set2))
###########
#operators
#########
#arithmatic
#a=10
#b=20
# print(f"add:{a+b},sub:{a-b},div:{a/b})")
#note:f""-->format the strings avoid + problems in large strings
#########
#/-->wll give exact division
#//-->will give only quotient
#in floor division if any one value is -ve the ans is lowest value
# a=10
# b=3
# print(a**b)
############
#comp assignment
# a=23
# b=10
# a+=b
# print(a)
#################
#comparison
# a=10
# b=20
# c=15
# print(a>b)
# print(a<=b)
# print(a>=b)
# print(a<b>=c)
#################
#logical
# a=0
# b=23
# c=45
# print(a and b and c)
# print(a or b or c)
# print(a or b and b or c)
# print(a-b or b-a or a-c or c-a)
############
#bitwise
# a=5
# b=10
# print(a&b)
# print(a|b)
# print(b>>1)
############
#membership
# list1=[1,2,3,4,5]
# a="hello world"
# print(2 in list1)
# print('w' in a)
# print("world" not in a)
#####################
#identity
# a=23
# b="23"
# print(type(a) is not type(int(b)))
################
# a=float(input("enter no"))
# print(a)
# print(type(a))
# a=float(input("enter no"))
# b=float(input("enter root"))
# c=a**(1/b)
# print(c)
####################
#decision making
# a,b=30,67
# if a>b:
# 	print("a is greater")
# else:
# 	print("b is greater")
# print("a is greater" if(a>b) else print("b is greate"))
###############
#elif statement
# a=10
# b=20
# c=30
# if a>b and a>c:
# 	print("a is greater")
# elif b>a and b>c:
# 	print("b is greater ")
# # else:
# # 	print("c is greater")
# print("a is greater") if a>b and a>c else print("b is greater") if b>a and b>c else print("c is greater")
#note=python doesnt supports switch
#############
#looping
#note=python doesnt support do while
# print(list(range(99,0,-2)))
# for i in "hello world":
# 	print(i)
# for i in "hello world":
# 	print(i,end=" ")
# for row in list(range(1,101)):
# 	for col in list(range(1,11)):
#     	print(row*col,end=" ")
# 		print()
# counter=0
# while counter<=10:
# 	print("hello all")
# 	counter+=1
# else:
# 	print("good bye")
#########
#control statements
# for i in "hello world":
# 		if i=="w":
# 			break
# 			print(i)

